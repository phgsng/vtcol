###############################################################################
                                     VTCOL
###############################################################################

Change the color scheme (and more) of the virtual Linux console.

Color schemes
-------------------------------------------------------------------------------

Use ``vtcol colors`` to manipulate the console palette.

.. image:: ./doc/img/solarized.png

**vtcol** loads color schemes from various source: From a set of predefined
palettes, from a binary dump or by loading it from a definition file. The
latter supports input in the format used by setcolors_. (Not much effort has
been put into ensuring compliance so YMMV.) Check the subdirectory
``./schemes`` in the **vtcol** tree for examples of definition files.

Four color schemes are predefined:

    * ``default``          the default color scheme of the Linux console.
    * ``solarized``        the Solarized_ color scheme, dark version.
    * ``solarized_light``  the Solarized_ color scheme, light version.
    * ``phosphor``         monochrome green text on black background.

Invoke **vtcol** with the ``set`` command specifying the scheme of your
choice: ::

    $ vtcol colors set solarized_light

Or for a more 80s look’n’feel: ::

    $ vtcol colors set phosphor

.. image:: ./doc/img/phosphor.png

In order to view the available schemes, use the ``list`` command. Should the
scheme specified not resolve to one of the predefined ones, **vtcol** will fall
back on interpreting the name as that of a file. Likewise, loading a scheme
directly from a definition file is accomplished by specifying the ``--file``
argument to ``set``. ::

    $ vtcol colors set --file ./schemes/solarized

Instead of an actual scheme or file name, these parameters accept ``-``
as an argument in order to read from ``stdin``. ::

    $ vtcol colors set -

To show the current scheme of the active console use the ``get`` subcommand: ::

    $ vtcol colors get
    solarized_dark

With the ``-base64`` switch ``vtcol`` outputs a binary representation of color
schemes: ::

    $ vtcol colors get --base64
    ACs23DIvhZkAtYkAJovS0zaCKqGY7ujVACs2y0sWWG51ZXuDg5SWbHHEk6Gh/fbj

which can then be re-applied with ``vtcol set``::

    $ vtcol colors set --base64 \
        ACs23DIvhZkAtYkAJovS0zaCKqGY7ujVACs2y0sWWG51ZXuDg5SWbHHEk6Gh/fbj

It is also possible to use vtcol to switch between two themes by means of the
``toggle`` subcommand. E. g. to cycle between “dark mode” and “light mode”: ::

    $ vtcol colors toggle solarized solarized_light

To view a scheme’s definition, for instance in order to verify that **vtcol**
parses it correctly, use the ``dump`` command. ::

    $ vtcol colors dump default
    $ vtcol colors dump ./schemes/solarized

This will print the color definitions contained in the scheme; if the specified
name does not resolve to a pre-defined scheme it will be interpreted as a file
name instead.

Keyboard LEDs and flags
-------------------------------------------------------------------------------

Keyboard handling is grouped under the ``vtcol kb`` subcommand.

Show the current state of the keyboard LEDs: ::

    $ vtcol kb leds get
    caps: false, num: true, scroll: false

Set the state of the Caps Lock LED (does *not* affect the Caps Lock state, just
the LED itself): ::

    $ vtcol leds set --caps on

Likewise for the keyboad state (*not* the LEDs):  ::

    $ vtcol kb flags get
    [flags: caps: false, num: false, scroll: false; default: caps: false, num: false, scroll: false]


Building
-------------------------------------------------------------------------------

Use Cargo to obtain a binary: ::

    $ cargo build --features=vtcol-bin

To generate the manpage, run ::

    $ make man

*rst2man* from the *Docutils* suite needs to be installed for this.

Background
-------------------------------------------------------------------------------

The default palette that comes with a Linux terminal was inherited from a long
history of virtual console implementations. The colors assigned were chosen for
pragmatic reasons but that palette may not harmonize with everybody’s taste.
Enter **vtcol**.

**vtcol** attempts to make the console colors themeable with a simple plain
text input format. The very popular themes from the Solarized_ family are
included as predefined palettes; the same is true of the Linux default palette,
so they can be conveniently restored when experimenting.

About
-------------------------------------------------------------------------------

The **vtcol** source code is available from the `canonical repository`_.
**vtcol** is redistributable under the terms of the `GNU General Public
License`_ version 3 (exactly). Patches or suggestions welcome.

.. _setcolors:                  https://github.com/EvanPurkhiser/linux-vt-setcolors
.. _Solarized:                  http://ethanschoonover.com/solarized
.. _GNU General Public License: http://www.gnu.org/licenses/gpl.txt
.. _canonical repository:       https://gitlab.com/phgsng/vtcol

