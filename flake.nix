{
  description = "vtcol development shell";

  inputs = {
    nixpkgs.url      = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    flake-utils.url  = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, rust-overlay, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
      in
      with pkgs;
      {
        devShells.default = mkShell {
          LD_LIBRARY_PATH = lib.makeLibraryPath [
            libGL
            libxkbcommon
            wayland
            xorg.libX11
            xorg.libXcursor
            xorg.libXi
            xorg.libXrandr
          ];

          buildInputs = [
            cmake
            fontconfig
            libglvnd
            openssl
            pkg-config
            pkgconfig
            rust-bin.beta.latest.default
            xorg.libX11
            xorg.libX11.dev
            xorg.libXcursor
            xorg.libXext
            xorg.libXft
            xorg.libXi
            xorg.libXrandr
            xorg.libXrender
            xorg.libXt
            xorg.xorgproto
            xorg.xorgserver
          ];
        };
      }
    );
}
