manpage		= doc/vtcol.1.gz
cargo-target 	= ./target
bin 		= $(cargo-target)/release/vtcol
lib 		= $(cargo-target)/release/libvtcol.rlib
bin-src 	= src/vtcol.rs
lib-src 	= src/lib.rs
src	 	= $(bin-src) $(lib-src)
meta 		= Cargo.toml
rustdoc-entry	= target/doc/vtcol/index.html
cargo-lock	= Cargo.lock
cargo-lock-patch= misc/nixos/pkgs/os-specific/linux/vtcol/cargo-lock.patch

all: bin lib doc nix

check: $(src)
	cargo test 

lib: $(lib)

bin: $(bin)

doc: man rustdoc

man: $(manpage)

rustdoc: $(rustdoc-entry)

nix: lockpatch

lockpatch: $(cargo-lock-patch)

$(cargo-lock-patch): $(meta)
	rm -f -- $(cargo-lock)
	cargo update
	cargo generate-lockfile
	mkdir -p tmp
	mv -f -- $(cargo-lock) tmp/
	diff -u /dev/null tmp/$(cargo-lock) >$(cargo-lock-patch) ; :

$(lib): $(lib-src) $(meta)
	cargo build --release 

$(bin): $(src) $(meta)
	cargo build --bin=vtcol --features=vtcol-bin --release 

$(manpage): doc/vtcol.rst
	rst2man $< |gzip > $@

$(rustdoc-entry): $(src) $(meta)
	cargo doc --bins

clean:
	rm -f -- $(manpage)
	rm -f -- $(cargo-lock)
	rm -rf -- $(cargo-target)
	rm -rf -- tmp

.PHONY: clean check
